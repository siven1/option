Ambiente utilizado para el test: 
- S.O Ubuntu 18.04

Herramientas instaladas en una partición desde 0:
- Kubectl
- Docker
- Minikube
- Git
- Jenkins

Lenguajes utilizados:
- Groovy
- Shell script
- JS

Repositorios utilizados:
- Proyecto Nodejs: https://gitlab.com/siven1/option.git  (Jenkinsfile+ Dockerfile+ yaml kubernetes).
- Scripts pipeline:  https://gitlab.com/matsiv/jenkins.git (Se documenta el código, stages+ métodos).
- Dockerhub:   https://hub.docker.com/r/sieven/option


Pasos:
1) Configuración sencilla de jenkins limpio desde el instalador (Siguiente, siguiente, instalar).
2) Instalar un único plugin: Pipeline Remote Loader Plugin (para cargar scripts de manera remota).
3) Dar permisos al usuario de jenkins dentro de ubuntu 18.04.
4) Creación de job multibranch dentro de la interfaz de Jenkins (Para ejecutar todas las ramas del repositorio según sea necesario).
![datei](fotos/4.png)
5) Configurar el job multibranch para el origen de datos desde repositorio de gitlab y desde el archivo Jenkinsfile dentro del mismo.
![datei](fotos/5.png)
![datei](fotos/5-2.png)
6) Ejecutar pipeline (Ejecución sin parámetros para ejecutar todos los stages, ejecución agregando un solo parámetro para un stage, ejecución con parámetros separados por punto y coma para todos los stages que se desea ejecutar).
![datei](fotos/6.png)

Notas:
- Para que funcione el acceso mediante ingress correctamente, se simula un dominio dentro de la tabla de hosts /etc/hosts, asociando la ip de minikube al dominio http://test.option/ y realizando el acceso directo a la raiz (Si no se realiza ese paso, se puede visualizar la app con $ipminikube+:31311).
ej: http://192.168.39.67:31311/



1) Si necesitaras poner esto en producción, ¿qué crees que podría faltar? ¿qué le agregarías si tuvieras más tiempo?

- En este ejercicio utilicé dockerhub como repositorio para fines prácticos, lo que genera que minikube haga un pull de las imagenes nuevas cada vez que se suben ahí, lo que es ineficiente para fines de agilidad o productivos. Para remediar esto emplearía un repositorio local (Además de que se agregaron las credenciales en duro dentro del código).

- Para temas prácticos todos los repositorios son de libre acceso, menos el de dockerhub. Por lo tanto lo ideal sería utilizar las medidas de seguridad adecuadas para código privado

- Agregaría scripts que limpiaran el ambiente de contenedores e imagenes que ya no se utilizan al ser redesplegadas.

- Al ser un proyecto front, agregaría stages que validen posibles códigos en duro o comentarios con palabras clave.








